---
id: hit-and-miss-1
title: naive hit and miss sampling
next: hit-and-miss-2
---
at the end of the day, we always use some form of hit and miss

 0. generate training samples, take note of maximal $|w|$
 1. generate event $\{p_i\}$
 2. calculate weight $w=\D\Phi\ \mathcal{M}$
 3. generate random number $r\in[0,1]$
 4. return $\{p_i\}$ and $\sigma(w)$ if $r < |w|/w_{\rm max}$

this is horribly inefficient!
