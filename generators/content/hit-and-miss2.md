---
id: hit-and-miss-2
title: next-to-naive hit and miss sampling
next: building-g
layout: two-column-image
image: https://mule-tools.gitlab.io/__static/gallery/idea.svg
---
if we can model our weight distribution somehow...

 0. generate training samples and build simple model distribution $|w| \le M g$ with $M$ as small as possible
 1. generate event $\{p_i\}$ using *g*
 2. calculate weight $w=\D\Phi\ \mathcal{M}$
 3. generate random number $r\in[0,1]$
 4. return $\{p_i\}$ and $\sigma(w)$ if $r < |w|/(Mg(x))$

