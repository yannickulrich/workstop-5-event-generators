---
title: cell resampling
id: cell
layout: two-column
next: cell2
---
 * cross sections are positive & experiments have finite resolution
 * algorithm: [[Andersen:2021mvw]]
     * choose *seed event* with $w<0$ to define a cell $\mathcal{C}$
     * add nearby events to $\mathcal{C}$ until $\displaystyle\sum_{i\in \mathcal{C}} w_i > 0$
     * reweight $w_i \to \frac{\sum_{j\in\mathcal{C}}w_j}{\sum_{j\in\mathcal{C}}|w_j|} |w_i| > 0$
     * repeat until all $w_i\ge0$ 
 * if largest cell size is bigger than experimental resolution, need more events
 * needs proper metric in event space. MUonE example: $d(e_1,e_2) = \sqrt{\big|\theta^e_1-\theta^e_2|^2+\big|\theta^\mu_1-\theta^\mu_2|^2}$

---

```animation
pattern: cres/cres_frame%07d.svg
nframes: 34
time:
    0..=7: 0
    8..=32: 1
    33..: 0
fps: 20
```
