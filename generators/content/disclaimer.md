---
title: disclaimer
id: disclaimer
start: true
next: integrators
layout: two-column-image
image: https://mule-tools.gitlab.io/__static/gallery/shower.svg
---

BIG disclaimer about me

 * I am **not** an expert on this
 * I have spoken to people who are
 * I have read some stuff
 * I have played with some code
 * but I am **not** an expert

BIG disclaimer about this talk

 * this is not a proper talk with results and plots
 * this is a miscellaneous collection of ideas
 * this is not the only way of doing things!
 * parton showers are a thing
