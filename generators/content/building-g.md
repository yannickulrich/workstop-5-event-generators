---
title: building $g$
id: building-g
next: negative
layout: two-column
---
easiest way: piecewise constant function

 * `vegas`: you can almost certainly already do that
 * *but* assumes factorising integrand
       $g(\vec x) = g_1(x_1) \times g_2(x_2) \times\cdots\times g_d(x_d)$
 * `foam`: split integration region by worst $M$ [[Jadach:2002kn]].

---

```animation
pattern: foam/frame%07d.svg
nframes: 129
time:
    0..4: 0
    5..=28: 2
    29..34: 0
    34..40: 2
    40..: 1
fps: 20
```
