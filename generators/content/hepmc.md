---
title: standards & tools
id: hepmc
next: questions
---
 * the high-energy community did a lot of work to standardise generators
    * [HepMC](http://hepmc.web.cern.ch/hepmc/)/[LHEF](http://home.thep.lu.se/~leif/LHEF/) for event storage
    * [Rivet](https://rivet.hepforge.org/) for experimental/theoretical validation
    * [Contur](https://hepcedar.gitlab.io/contur-webpage/) for BSM constraints
    * [YODA](https://yoda.hepforge.org/) for data analysis
 * McMule will provide a HepMC/LHEF interface, probably a rivet one as well
 * there aren't that many [public rivet analysis from this community](https://rivet.hepforge.org/rivet-coverage#ee-lowexpt) 😢
