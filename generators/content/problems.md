---
id: problems
title: why this doesn't work
next: hit-and-miss-1
layout: two-column-image
image: https://mule-tools.gitlab.io/__static/gallery/thinking.svg
---

 * $|w|$ spans many order of magnitude (even after `vegas` adaption)
 * $w$ and can be negative beyond LO
 * can't define a negative pdf

**why do we want $w\sim1$?**

 * for theorists $S(\{p_i\})$ is trivial $\ll 1{\rm ms}$
 * but it might be a full detector simulation $\gtrsim 1{\rm h}$
 * want to minimise number of events passing through $S$
 * have cancellation early rather than late

