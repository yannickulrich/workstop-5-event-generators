---
title: origin of negative weights
id: negative
next: cell
---
```math
\sigma_{\rm NLO} &=
    \int\D\sigma_n^{(0)}
    + \frac{\alpha}{4\pi}\int\D\sigma_n^{(1)}
    + \frac{\alpha}{4\pi}\int\D\sigma_{n+1}^{(0)}
    \\[1em]&
    = \int\underbrace{
        \Big(\D\sigma_n^{(0)} + \frac{\alpha}{4\pi}\D\sigma_n^{(1)} + \text{soft}(\omega_c)\Big)
    }_{\text{mostly $>0$}}
    + \frac{\alpha}{4\pi}\int_{\omega>\omega_c}\underbrace{\D\sigma_{n+1}^{(0)}}_{>0}
    \\[1em]&
    = \int\underbrace{\Big(
        \D\sigma_n^{(0)}
        + \frac{\alpha}{4\pi}\D\sigma_n^{(1)} + \frac\alpha{4\pi}\int\D{\rm CT}\Big)
    }_{\text{mostly $>0$}}
    + \frac{\alpha}{4\pi}\int\underbrace{\Big(\D\sigma_{n+1}^{(0)}-\D{\rm CT}\Big)}_\text{whatever}
```

 * slicing: fairly few negative weights **but** numerically construct $\log(\omega_c)$
 * subtraction: stable integration **but** lots and lots of negative weights

*if $r\times N$ events are negative, you need $\propto 1/(1-2r)^2$ events*
