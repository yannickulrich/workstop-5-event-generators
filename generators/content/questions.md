---
title: questions
id: questions
layout: two-column-image
image: https://mule-tools.gitlab.io/__static/gallery/work.svg
---
 * Q: are there better samplers? well, *duh*
 * Q: is it worth using them?
 * Q: how does subtraction / cell resampling compare to slicing?
 * Q: is the added complexity worth it?
 * Q: how to improve resampling efficiency?
 * Q: this was designed for NLO, how does it cope with higher orders?
