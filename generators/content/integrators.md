---
id: integrators
title: event generators vs. integrators
next: problems
layout: two-column-image
image: https://mule-tools.gitlab.io/__static/gallery/garden.svg
---
<div>

 * most naive theory code are integrators, i.e. they can calculate
    $\displaystyle\sigma = \int \D\Phi\ \mathcal{M} \ S(\{p_i\})$
    for arbitrary measurement functions $S$
 * event generators can generate events distributed according to $\D\Phi\ \mathcal{M}$
 * trivial event generator: dump every event $\{p_i\}$ and weight $w=\D\Phi\ \mathcal{M}$ to file

</div>
