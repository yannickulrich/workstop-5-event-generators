---
title: cell resampling
id: cell2
layout: two-column-image
image: https://mule-tools.gitlab.io/__static/gallery/soap.svg
next: hepmc
---
 * need efficient way of nearest neighbour search with arbitrary metric
 * idea: [vantage-point tree](https://fribbels.github.io/vptree/writeup)
 * note: this means that $d(e_1,e_2)$ is a proper metric with a triangle inequality
 * fixing maximal distance makes this super efficient
 * you can *sample the tree* rather than the original foam when you need more events
 * `foam+cres+VPsearch`: it's bubbles all the way down
