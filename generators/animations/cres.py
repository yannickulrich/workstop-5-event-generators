import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import os

try:
    os.mkdir('cres_frames')
except FileExistsError:
    pass


def moving_average(a, n=3) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


# the PDF is $d\sigma/d\Omega ~ \frac38 (1+c^2)$  where $c=\cos\theta$.
# the inverse CDF is
def invcdf(C):
    D = (-2 + 4*C + np.sqrt(5 - 16*C + 16*C**2))**(1./3.)
    return D - 1/D


def sample(r):
    c = invcdf(r[:,0])
    return np.column_stack((
        np.arccos(c),
        2*np.pi*r[:,1]
    ))


np.random.seed(0)
dat = sample(np.random.uniform(size=(400,2))) / [np.pi,2*np.pi]
weights = np.random.normal(loc=0.4,size=(400))


fig = plt.figure(figsize=(6,6))
sp = plt.scatter(
    dat[:,0],dat[:,1], s=10, c=weights, cmap='Spectral'
)
plt.xlabel(r'$\theta/\pi$')
plt.ylabel(r'$\phi/2\pi$')

frame = 0
fig.savefig(f'cres_frames/cres_frame{frame:07d}.svg')

cell = patches.Circle([0,0], 0, alpha=0.3, color='red')
plt.gca().add_patch(cell)

for _ in range(3):
    seed = np.argmin(weights)
    distances = np.sqrt(np.sum((dat-dat[seed])**2,1))
    args = np.argsort(distances)
    sizes = moving_average(distances[args], 2)

    cell.set_center(dat[seed])

    for n, (size, wgt) in enumerate(zip(sizes, np.cumsum(weights[args])[:-1])):
        cell.set_radius(size)
        plt.gca().set_title(
            f'$w={wgt:.2f}$, $r(\mathcal{{C}})={size:.2f}$'
        )
        frame = frame + 1
        fig.savefig(f'cres_frames/cres_frame{frame:07d}.svg')
        if wgt > 0:
            wgts = weights[args[:n+1]]
            weights[seed] = np.sum(wgts)/np.sum(np.abs(wgts)) * np.abs(weights[seed])
            sp.set_array(weights)
            sp.stale = True
            cell.set_radius(0)
            frame = frame + 1
            fig.savefig(f'cres_frames/cres_frame{frame:07d}.svg')
            break
