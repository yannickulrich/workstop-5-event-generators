import numpy as np


class Cell:
    def __init__(self, pos, size):
        self.pos = pos
        self.size = size
        self.d = len(self.pos)
        self.active = True

    def sample(self, func, nsamples):
        self.lam = np.random.rand(nsamples, self.d)
        self.samples = func((self.pos + self.size * self.lam).T)
        self.probability = np.max(self.samples)
        self.abssum = np.mean(self.samples)

    def find_cut(self, axis, Nb):
        # consider all pairs (i,j) with 0 <= i <= j <= Nb and
        # calculate the ceiling for all of them
        histogram = self.histograms[axis]
        def ceiling(i, j):
            return (j-i+1)/Nb * (np.max(histogram) - np.max(histogram[i:j+1]))

        best = (None, None)
        bestv = -np.inf
        for i in range(0, Nb):
            for j in range(i, Nb+1):
                v = ceiling(i, j)
                if v > bestv:
                    bestv = v
                    best = (i, j)

        i, j = best
        if i == 0:
            return j / Nb
        elif j == Nb:
            return i / Nb
        else:
            return i / Nb

    def split(self, Nb):
        # Begin by histogramming the data
        self.histograms = np.array([
            np.histogram(
                self.lam[:,i], weights=self.samples,
                bins=Nb, range=[0,1]
            )[0]
            for i in range(self.d)
        ])

        # Calculate the score for each axis
        self.axis_score = np.sum(
            1 - self.histograms.T / np.max(self.histograms,1),
            0
        )
        # and find the best
        axis = np.argmax(self.axis_score)

        # now find where to cut
        lam_cut = self.find_cut(axis, Nb)

        oneat = np.zeros_like(self.size)
        oneat[axis] = 1

        return [
            Cell(self.pos, lam_cut**oneat * self.size),
            Cell(self.pos + oneat * lam_cut * self.size, (1-lam_cut)**oneat * self.size)
        ]


def build_foam(func, ndim, itmx, calls=500, Nb=8):
    root = Cell(np.zeros(ndim), np.ones(ndim))
    root.sample(func, calls)
    cells = [root]

    while len(cells) < itmx:
        ind = max([
                (n,i.probability - i.abssum)
                for n,i in enumerate(cells)
            ],
            key=lambda a:a[1]
        )[0]
        cell = cells.pop(ind)
        cell.active = False
        d1, d2 = cell.split(Nb)
        d1.sample(func, calls)
        d2.sample(func, calls)
        cell.daughters = [d1,d2]
        cells += [d1, d2]
        yield (d1,d2)


def random_cell(cells, n=None):
    probs = np.array([i.probability for i in cells])
    probs /= np.sum(probs)
    return np.random.choice(cells, size=n, p=probs)


def animate_growth(func):
    import matplotlib.animation as animation
    import matplotlib.pyplot as plt
    X = np.linspace(0,1)
    Y = np.linspace(0,1)
    X, Y = np.meshgrid(X, Y)
    Z = func([X,Y])
    all_cells = []

    def animator(cells):
        nonlocal all_cells
        d1, d2 = cells
        all_cells += [d1,d2]

        d = d2.pos-d1.pos
        if d[0] == 0:
            # hline
            return [plt.axhline(d2.pos[1], d2.pos[0], d2.pos[0]+d2.size[0], color='black')]
        else:
            # vline
            return [plt.axvline(d2.pos[0], d2.pos[1], d2.pos[1]+d2.size[1], color='black')]


    fig = plt.figure( figsize=(6,6) )

    plt.imshow(
        Z,
        interpolation='bilinear', cmap='PuBuGn',
        origin='lower', extent=[0,1,0,1]
    )

    return fig, animation.FuncAnimation(
        fig,
        animator,
        frames=build_foam(func, 2, 30)
    ), all_cells


def animate_sampling(fig, cells, n):
    import matplotlib.animation as animation
    import matplotlib.pyplot as plt
    import matplotlib.patches as patches
    rect = patches.Rectangle([0,0], 1,1, alpha=0.3, color='red')
    dots = plt.plot([], [], 'r.')
    plt.gca().add_patch(rect)

    samples = random_cell([i for i in cells if i.active],n)
    pos = np.array([i.pos for i in samples])
    size = np.array([i.size for i in samples])
    points = pos + np.random.uniform(size=(n,2)) * size

    def animator(arg):
        if isinstance(arg, Cell):
            rect.set_bounds((arg.pos[0],arg.pos[1], arg.size[0], arg.size[1]))
            return rect
        else:
            dots[0].set_xdata(points[:arg + 1,0])
            dots[0].set_ydata(points[:arg + 1,1])
            return dots

    return animation.FuncAnimation(
        fig,
        animator,
        frames=[j for a,b in zip(samples, range(len(points))) for j in [a,b]],
        interval=1000
    )


if __name__ == "__main__":
    import matplotlib.animation
    class MyWriter(matplotlib.animation.HTMLWriter):
        def __init__(self, offset):
            super().__init__()
            self.frame_format = 'svg'
            self.offset = offset

        def setup(self, fig, outfile, dpi=None, frame_dir=None):
            super().setup(fig, outfile, dpi, frame_dir)
            self._frame_counter += self.offset


    func = lambda x : 5.626976975981913*np.exp((
        -(-0.3 + x[0])*(75.*(-0.3 + x[0]) - 25.*(-0.6 + x[1]))
        -(-25*(-0.3 + x[0]) + 25*(-0.6 + x[1]))*(-0.6 + x[1])
        )/2.)

    writer = MyWriter(0)

    fig, anim, cells = animate_growth(func)
    anim.save('foam.html', writer=writer)

    writer = MyWriter(writer._frame_counter)
    anim = animate_sampling(fig, cells, 50)
    anim.save('foam.html', writer=writer)
