import transparencies.plugins
import pyqrcode
import base64
import io


class qr_renderer(transparencies.plugins.block_renderer):
    def can_render(self, token):
        return token.language == 'qr'

    def render_block_code(self, token):
        code = token.children[0].content
        qr = pyqrcode.create(code)
        fp = io.BytesIO()
        qr.svg(fp)
        fp.seek(0)
        svg = base64.b64encode(fp.read()).decode()
        return f'<div class="image"  style="background-image: url(\'data:image/svg+xml;base64,{svg}\'); width: 400px; height: 400px;"></div>'


main = qr_renderer
