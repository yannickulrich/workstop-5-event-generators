---
id: conference
title: satelite conference
next: dinner
layout: two-column
---
 * we will have a broader hybrid conference on Wed/Thu/Fri afternoon
 * this is to inform others of our activities ('outreach')
 * and to learn what others are doing that may be useful
 * provide context
 * this will not take place here but in [KOL-F-101](https://plaene.uzh.ch/KOL/room/KOL-F-101)
 * take the 9 or 10 tram to *ETH/Universit&auml;tsspital*

---

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2888.8797610944785!2d8.548353986226424!3d47.375570002202466!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479aa09f5895858b%3A0xba50fa52d07edacf!2sUniversity%20of%20Zurich!5e0!3m2!1sen!2suk!4v1685822780929!5m2!1sen!2suk" width="400" height="500" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
