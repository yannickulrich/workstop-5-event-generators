---
id: workstop
title: "workstop / thinkstart"
next: work
---

this is not a work**shop** but a work**stop**
 * we are here to work and start thinking, not to read emails
 * ideally have a compare-and-contrast write-up
 * 5<sup>th</sup> edition, previous installations in Zurich, at GGI, and IPPP
   * 2016: new collaborative understanding of dimensional regularisation [To d or not to d, 2017](https://inspirehep.net/literature/1598144)
   * 2019: progress report and future planning of the MUonE Theory Initiative [Theory for muon-electron scattering @ 10 ppm, 2020](https://inspirehep.net/literature/1793261)
   * 2019: four-dimensional regularisation at NNLO (GGI) [May the four be with you, 2020](https://inspirehep.net/literature/1835097)
   * 2022: Towards N<sup>3</sup>LO (Durham)
