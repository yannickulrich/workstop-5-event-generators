---
id: welcome
title: welcome to Zurich!
next: workstop
start: true
layout: two-column
---

*A warm welcome to Zurich on behalf of the Organising Committee*
([Yannick Ulrich](mailto:yannick.ulrich@durham.ac.uk),
[Adrian Signer](https://inspirehep.net/authors/988723),
[Andrzej Kupsc](https://inspirehep.net/authors/988723),
[Graziano Venanzoni](https://inspirehep.net/authors/988723))

 * please upload your slides or send them to me
 * if you haven't already, please join the Slack `gamma-star.slack.com`
 * please make sure you got your lunch vouchers
 * tea & coffee breaks will be provided here so we can keep discussing
 * official conference dinner on Thursday, paid for by us
 * please read and follow the [Code of Conduct](https://indico.psi.ch/event/13707/page/2487-code-of-conduct)

---

<style>
svg { scale: 8; }
</style>

```qr
https://indico.psi.ch/event/13707
```

<center>
<a href="https://indico.psi.ch/e/gamma-star-workstop"><code>indico.psi.ch/e/gamma-star-workstop</code></a>
</center>
