---
id: work
title: how will this work?
next: conference
---

we are **not** here to give talks
 * each session is 4h and about one topic
 * we start with *O*(1h) of joint talks
 * these will overrun and that's fine
 * please be honest!

we are here to work!
 * after 1h coffee will be delivered
 * discussion from the talk will evolve into a blackboard discussion (with tea and snacks)
 * playing with code is encouraged (after the talks)!
 * please don't just read your emails!
