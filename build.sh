#!/bin/sh
set -ex

build_animations()
{
    cd generators/animations
    python foam.py
    python cres.py
    cd ../..
}

build_generators()
{
    mkdir -p public/generators
    cd generators
    transparencies head.yaml
    cd ..
    cp generators/public/* public/generators || true
    cp -R generators/animations/foam_frames public/generators/foam/
    cp -R generators/animations/cres_frames public/generators/cres/
}

build_n3lo()
{
    mkdir -p public/n3lo
    cd n3lo
    transparencies head.yaml
    cd ..
    cp n3lo/public/* public/n3lo || true
}

build_welcome()
{
    if [ ! -d "welcome/pyqrcode" ]; then
        mkdir -p welcome/pyqrcode
        for i in __init__.py builder.py tables.py; do
            wget https://raw.githubusercontent.com/mnooner256/pyqrcode/master/pyqrcode/$i -O welcome/pyqrcode/$i
        done
    fi

    mkdir -p public/welcome
    cd welcome
    TRANSPARENCIES_PLUGIN_PATH=. transparencies head.yaml
    cd ..
    cp welcome/public/* public/welcome || true
}

build_website()
{
    mkdir -p public
cat <<EOF > public/index.html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <style type="text/css">
            html * {
                font-family: Georgia, sans-serif;
            }
            #content{
                max-width: 35rem;
                margin: auto auto 2rem;
                padding: 1rem 1rem 0px;
            }
            #content a{
                text-decoration: none;
                border-bottom: 1px dotted rgb(0, 75, 107);
                color: rgb(0, 75, 107);
            }
            h1,h2,h3,h4,h5,h6 {
                font-weight: normal;
                margin: 30px 0px 10px 0px;
                padding: 0px;
                text-align: left;
            }

            h1 { font-size: 240%; margin-top: 0px; padding-top: 0px; }
            h2 { font-size: 180%; }
            blockquote{
                font-size: inherit;
                color: #535158;
                padding-top: 0.5rem;
                padding-bottom: 0.5rem;
                padding-left: 1.5rem;
                margin-top: 0.5rem;
                margin-bottom: 0.5rem;
                margin-left: 0;
                margin-right: 0;
                box-shadow: inset 4px 0 0 0 #dcdcde;
            }

            svg {
                margin: 0 auto;
                width: 100%;
            }
        </style>
        <title>Talks at the 5th Workstop/Thinkstart</title>
    </head>
    <body>
        <div id="content">
EOF
cat strong2020.svg >> public/index.html
mistletoe README.md >> public/index.html
cat <<EOF >> public/index.html
        </div>
    </body>
</html>
EOF

}


case "$1" in
    animation)
        rm -rf public/
        build_animations
        ;;

    n3lo)
        build_n3lo
        ;;

    generators)
        build_generators
        ;;

    welcome)
        build_welcome
        ;;

    talks)
        build_generators
        build_n3lo
        build_welcome
        ;;

    index)
        build_website
        ;;

    all)
        rm -rf public/
        build_animations
        build_generators
        build_n3lo
        build_website
        build_welcome
esac
