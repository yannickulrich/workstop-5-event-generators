# 5<sup>th</sup> Workstop / Thinkstart

These are talks I have presented at the [5<sup>th</sup> Workstop / Thinkstart](https://indico.psi.ch/event/13707/):

 * Talk 0, [*Welcome*](https://yannickulrich.gitlab.io/workstop-5-talks/welcome): the welcome talk
 * Talk 1, [*From Cross Sections to Events: An amateur's guide*](https://yannickulrich.gitlab.io/workstop-5-talks/generators): an naive introduction to event generators with a focus on fixed-order calculations using [foam](http://jadach.web.cern.ch/jadach/Foam/Index.html) and [cres](https://github.com/a-maier/cres)
 * Talk 2, [*Towards N<sup>3</sup>LO: overview*](https://yannickulrich.gitlab.io/workstop-5-talks/n3lo): an overview talk about the status of the N<sup>3</sup>LO calculation

> In this workstop, we will discuss radiative corrections and Monte Carlo tools for low-energy hadronic cross sections in <i>e<sup>+</sup>e<sup>-</sup></i> collisions.
> This is to be seen as part of the Strong 2020 effort. We will cover
> 
>   * leptonic processes at NNLO and beyond
>   * processes with hadrons
>   * parton shower
>   * experimental input
> 
> Each area will be given at least half a day, starting with an open 1h seminar followed by a lengthy discussion.
> 
> Just like previous workstops, this is an in-person event.
> We try to gather a small number of theorists who actively work on this topic to make very concrete progress.
> It should not just be about giving talks, but to actually learn from each other and put together the jigsaw pieces.
