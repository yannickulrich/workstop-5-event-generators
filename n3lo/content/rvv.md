---
title: RVV
id: rvv
next: vvv
---
 * probably the most complicated
 * without full mass dependence:
    * massless result in principle known (3j @ NNLO [[Gehrmann:2000zt]] [[Gehrmann:2001ck]])
    * recomputed by Turin group [[local:turinrvv]]
    * requires tricks (see [Tim's talk](https://indico.psi.ch/event/13707/contributions/44572/) [[Engel:2021ccn]] [[Engel:2023ifn]])
    * Q: jettification at two-loop
 * with full mass dependence:
    * analytic: almost certainly not possible in the foreseeable future
    * numeric: *maybe...*, reduction looks doable, DiffExp [[Hidding:2020ytt]] / AMFlow [[Liu:2022chg]] for evaluation
    * Q: requires good basis
