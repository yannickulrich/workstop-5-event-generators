---
title: VVV
id: vvv
next: questions
---
 * a lot of interesting maths studied for analytic
 * semi-numeric result (deep expansions) now available [[Fael:2022rgm]] [[Egner:2022jot]] [[Fael:2022miw]] [[Fael:2023zqr]]
 * [fast implementation](https://gitlab.com/formfactors3l/ff3l) using Chebyshev expansions
 * see [Fabian's talk](https://indico.psi.ch/event/13707/contributions/44571/)
 * Q: how to convert schemes / develop massification at three-loop
