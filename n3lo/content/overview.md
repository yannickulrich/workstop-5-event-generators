---
title: overview
id: overview
start: true
next: workstop4
---

 * ${\rm FKS}^\ell$ is a subtraction scheme that will work at N<sup>3</sup>LO [[Engel:2019nfw]]
 * 'just' need the matrix elements 😁
 * impossible in most cases, plausible for $ee\to\gamma^\ast$
   * VVV: HQFF, known [[Fael:2022rgm]] [[Egner:2022jot]] [[Fael:2022miw]] [[Fael:2023zqr]]
   * RVV: known for massless electrons from the Turin group as $\bar u\cdots \gamma^\mu \cdots u$ (talk on Thursday by [Ryan Moodie](https://indico.psi.ch/event/13708/contributions/43409/))
   * RRV: OpenLoops [[Buccioni:2017yxi] [[Buccioni:2019sur]]
   * RRR: 'trivial'
