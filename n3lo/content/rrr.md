---
title: RRR
id: rrr
next: rrv
---
<div>

 * tree level calculation, though with quite a few particles
 * should be trivial to calculate and subtract
 * stability may be delicate

**suggestion**: use spinor-helicity amplitude to get $\mathcal{A}^\mu = \bar u(p_1) \cdots \gamma^\mu \cdots u(p_2)$

**pro**:
 * current can be contracted with anything
 * highly symmetric process, symmetrised in Fortran

**con**:
 * spinor amplitudes are a bit messy with massive particles
 * probably need many implementations to compare stability

</div>
