---
title: "<s>summary</s> questions"
id: questions
---

 * Q: what's the best way to do RRR?
 * Q: how to do two soft photons to NLP?
 * Q: can we also do a current in OpenLoops?
 * Q: jettification at two-loop
 * Q: good basis for RVV
 * Q: how to convert schemes / develop massification at three-loop
 * Q: connection to generator
