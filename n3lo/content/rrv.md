---
title: RRV
id: rrv
next: rvv
---
 * OpenLoops can provide $ee\to\gamma\gamma^\ast(\to f\bar f)$ [[Buccioni:2017yxi] [[Buccioni:2019sur]]
 * Q: can we also do a current?
 * increase speed / stability by expanding for $E_\gamma \lesssim 10^{-3}\sqrt{s}/2$ to NLP
 * clear how to do for one soft [[Engel:2021ccn]]
 * Q: how to do two soft photons to NLP?
 * Tim will talk about this a bit more
